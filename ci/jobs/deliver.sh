#!/usr/bin/env bash

set -x -o errexit -o nounset -o pipefail

CONTAINER_IMAGE=$(./ci/custom/get_container_image_run.sh)
PROJECT_NAME=$(./ci/custom/get_project_name.sh)
PROJECT_VERSION=$(./ci/utils/get_project_version.sh)
RELEASE_TAG="${PROJECT_NAME}-${PROJECT_VERSION}"



# grant docker login
echo "${DOCKER_PASSWORD}" | buildah login --username "${DOCKER_USERNAME}" --password-stdin "${DOCKER_REGISTRY_URL}"

# grant to not replace existing image tag, except for master tags
if test "${PROJECT_VERSION}" != "master"
then
    buildah from --tls-verify=false "${DOCKER_REGISTRY_URL}:${RELEASE_TAG}" &> /dev/null && echo "ERROR: image \"${DOCKER_REGISTRY_URL}:${RELEASE_TAG}\" already exists" && exit 1
fi



# build images
export STORAGE_DRIVER=overlay # Force to use overlay fs, cannot be run in rootless containers https://docs.gitlab.com/ee/ci/docker/buildah_rootless_tutorial.html#configure-the-job

# from https://fediverse.blog/~/Admin@blog.asonix.dog/cross-buliding-containers-with-qemu-user-static-and-buildah
CONTAINER=$(buildah from --tls-verify=false docker.io/multiarch/qemu-user-static:latest)
buildah run --cap-add CAP_SYS_ADMIN --isolation chroot "${CONTAINER}" /register --reset -p yes

buildah manifest create multiarch

buildah bud --force-rm --no-cache --squash --format=docker \
        --platform linux/amd64 \
        --tag "${DOCKER_REGISTRY_URL}:${RELEASE_TAG}" \
        --tag "${DOCKER_REGISTRY_URL}:${PROJECT_NAME}-latest" \
        --build-arg "FROM_IMAGE=${CONTAINER_IMAGE=}" \
        --build-arg "BUILD_DIR=${BUILD_DIR_AMD64=}" \
        --manifest multiarch \
        --file ci/custom/Dockerfile_amd64 .

buildah bud --force-rm --no-cache --squash --format=docker \
        --platform linux/arm64/v8 \
        --tag "${DOCKER_REGISTRY_URL}:${RELEASE_TAG}" \
        --tag "${DOCKER_REGISTRY_URL}:${PROJECT_NAME}-latest" \
        --build-arg "FROM_IMAGE=${CONTAINER_IMAGE=}" \
        --build-arg "BUILD_DIR=${BUILD_DIR_ARM64V8=}" \
        --manifest multiarch \
        --file ci/custom/Dockerfile_arm64v8 .

buildah manifest push --tls-verify=false --all --format v2s2 multiarch "docker://${DOCKER_REGISTRY_URL}:${RELEASE_TAG}"

# create a latest image tag, except for the master tags
if test "${PROJECT_VERSION}" != "master"
then
    buildah manifest push --tls-verify=false --all --format v2s2 multiarch  "docker://${DOCKER_REGISTRY_URL}:${PROJECT_NAME}-latest"
fi
