#!/usr/bin/env bash

set -x -o errexit -o nounset -o pipefail

mkdir -m 777 -p ${CACHE_DIR} ${BUILD_DIR}

CONTAINER_IMAGE=$(./ci/custom/get_container_image_build.sh)

# prepare a container from the defined image
CONTAINER=$(buildah from --tls-verify=false "${CONTAINER_IMAGE}")



# cross-build the code isolatedly
buildah run \
       --volume "$(pwd)/${CACHE_DIR}:/srv/cache:rw" \
       --volume "$(pwd)/${BUILD_DIR}:/srv/build:rw" \
       --volume "$(pwd):/srv/repo_host:ro" \
       "${CONTAINER}" \
       /bin/bash -c \
       "cp -a /srv/repo_host /srv/repo; cd /srv/repo; ./ci/custom/internal_build_crosscompile.sh"
