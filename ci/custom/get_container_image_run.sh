#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

IMAGE="docker.io/fluent/fluent-bit:3.2.8"

echo "${IMAGE}"
